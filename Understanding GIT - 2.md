# Merging strategies
##  Fast forward merge
![](https://media.geeksforgeeks.org/wp-content/uploads/20191231172842/Fast-Forward-Merge.png)
##  Recursive merge
![](https://media.geeksforgeeks.org/wp-content/uploads/20191231173638/Recursive-Merge.png)

#   Rebase

Rebasing is the process of moving or combining a sequence of commits to a new base commit.
![](https://wac-cdn.atlassian.com/dam/jcr:e4a40899-636b-4988-9774-eaa8a440575b/02.svg?cdnVersion=1587)
```
git rebase <base>
```
----

# Git Reset
-   The git reset command is a complex and versatile tool for undoing changes. 
-   It has three primary forms of invocation. These forms correspond to command line arguments --soft, --mixed, --hard. The three arguments each correspond to Git's three **internal state management** mechanism, The Commit Tree (HEAD), The Staging Index, and The Working Directory.
![](https://wac-cdn.atlassian.com/dam/jcr:b02e7b60-742a-449d-921d-53c32410576d/git-sequence-transparent.png?cdnVersion=1587)

`$ git reset b`
![](https://wac-cdn.atlassian.com/dam/jcr:29e29d3d-dddd-480b-afd9-77169a7b0230/git-reset-transparent.png?cdnVersion=1587)

![](https://wac-cdn.atlassian.com/dam/jcr:7fb4b5f7-a2cd-4cb7-9a32-456202499922/03%20(8).svg?cdnVersion=1587)

## **--hard**
-   This is the most direct, DANGEROUS.
-   Frequently used option. 
-   When passed --hard The Commit History ref pointers are updated to the specified commit. Then, the Staging Index and Working Directory are reset to match that of the specified commit.
```
$ echo 'new file content' > new_file
$ git add new_file
$ echo 'changed content' >> reset_lifecycle_file    //reset_lifecycle_file already in the staging area

$ git status
On branch master
Changes to be committed:
   (use "git reset HEAD ..." to unstage)

new file: new_file

Changes not staged for commit:
   (use "git add ..." to update what will be committed)
   (use "git checkout -- ..." to discard changes in working directory)

modified: reset_lifecycle_file

$ git ls-files -s
100644 8e66654a5477b1bf4765946147c49509a431f963 0 new_file
100644 d7d77c1b04b5edd5acfc85de0b592449e5303770 0 reset_lifecycle_file
```
Let us now execute a git reset --hard and examine the new state of the repository.

```
$ git reset --hard
HEAD is now at dc67808 update content of reset_lifecycle_file
$ git status
On branch master
nothing to commit, working tree clean
$ git ls-files -s
100644 d7d77c1b04b5edd5acfc85de0b592449e5303770 0 reset_lifecycle_file
```
## **--mixed**
-   This is the **default** operating mode. 
-   The ref pointers are updated. The Staging Index is reset to the state of the specified commit.  
```
$ echo 'new file content' > new_file
$ git add new_file
$ echo 'append content' >> reset_lifecycle_file
$ git add reset_lifecycle_file
$ git status
On branch master
Changes to be committed:
    (use "git reset HEAD ..." to unstage)

new file: new_file
modified: reset_lifecycle_file


$ git ls-files -s
100644 8e66654a5477b1bf4765946147c49509a431f963 0 new_file
100644 7ab362db063f9e9426901092c00a3394b4bec53d 0 reset_lifecycle_file
```
```
$ git reset --mixed
$ git status
On branch master
Changes not staged for commit:
    (use "git add ..." to update what will be committed)
    (use "git checkout -- ..." to discard changes in working directory)

modified: reset_lifecycle_file

Untracked files:
    (use "git add ..." to include in what will be committed)

new_file


no changes added to commit (use "git add" and/or "git commit -a")
$ git ls-files -s
100644 d7d77c1b04b5edd5acfc85de0b592449e5303770 0 reset_lifecycle_file
```
## **--soft**

-   When the --soft argument is passed, the ref pointers are updated and the **reset stops there**. 
-   Staging and working directory are **untouched**
```
$ git add reset_lifecycle_file 

$ git ls-files -s 
100644 67cc52710639e5da6b515416fd779d0741e3762e 0 reset_lifecycle_file

$ git status 
On branch master 
Changes to be committed: 
(use "git reset HEAD ..." to unstage) 
modified: reset_lifecycle_file 
Untracked files: 
(use "git add ..." to include in what will be committed) 
new_file
```
With the repository in this state we now execute a soft reset.
```
$ git reset --soft
$ git status
On branch master
Changes to be committed:
    (use "git reset HEAD ..." to unstage)

modified: reset_lifecycle_file
$ git ls-files -s
100644 67cc52710639e5da6b515416fd779d0741e3762e 0 reset_lifecycle_file
```
To better understand and utilize --soft we need a target commit that is not HEAD. We have reset_lifecycle_file waiting in the Staging Index. Let's create a new commit.
```
$ git commit -m"prepend content to reset_lifecycle_file"
```
At this point, our repo should have three commits. We will be going back in time to the first commit. 
Before we travel back in time lets first check the current state of the repo.
```
$ git status && git ls-files -s
On branch master
nothing to commit, working tree clean
100644 67cc52710639e5da6b515416fd779d0741e3762e 0 reset_lifecycle_file
```
With this in mind lets execute a soft reset back to our first commit.
```
$git reset --soft 780411da3b47117270c0e3a8d5dcfd11d28d04a4
$ git status && git ls-files -s
On branch master
Changes to be committed:
    (use "git reset HEAD ..." to unstage)

modified: reset_lifecycle_file
100644 67cc52710639e5da6b515416fd779d0741e3762e 0 reset_lifecycle_file
```
----
