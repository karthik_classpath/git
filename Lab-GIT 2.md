#   Merging strategies
## Creating 2 new branches
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git branch branch-one

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git branch branch-two

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git branch
  branch-one
  branch-two
* master
```
## Checkout to branch one, edit existing file and add new file.

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git checkout branch-one
Switched to branch 'branch-one'
```
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git status
On branch branch-one
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   example

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        Branch-one.txt
```

##  Add and commit these changes

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git commit -m "Commiting branch one with changes"
[branch-one a076e58] Commiting branch one with changes
 2 files changed, 3 insertions(+), 1 deletion(-)
 create mode 100644 Branch-one.txt
```

## Merge with master - Fast forward

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git checkout master
Switched to branch 'master'

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git merge branch-one
Updating f224d14..a076e58
Fast-forward
 Branch-one.txt | 1 +
 example        | 3 ++-
 2 files changed, 3 insertions(+), 1 deletion(-)
 create mode 100644 Branch-one.txt
```

##  Checkout to the second branch, edit the new file created and create another file
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git checkout branch-two
Switched to branch 'branch-two'

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-two)
$ git status
On branch branch-two
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        Branch-one.txt
        Branch-two.txt

nothing added to commit but untracked files present (use "git add" to track)
```

##  Add and commmit these changes

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-two)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-two)
$ git commit -m "Commiting second branch with changes"
[branch-two 59153fc] Commiting second branch with changes
 2 files changed, 2 insertions(+)
 create mode 100644 Branch-one.txt
 create mode 100644 Branch-two.txt
```

## Merge with master - Recursive

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-two)
$ git checkout master
Switched to branch 'master'

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git merge branch-two
CONFLICT (add/add): Merge conflict in Branch-one.txt
Auto-merging Branch-one.txt
Automatic merge failed; fix conflicts and then commit the result.

```
## Resolve conflicts and then commit

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master|MERGING)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master|MERGING)
$ git commit -m "After resolving commiting"
[master 7adadc5] After resolving commiting
```

--------------
--------------

#   Rebase

##  Make changes on master and commit
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   Branch-one.txt
```
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git commit -m "Changes in master"
[master 827e9f0] Changes in master
 1 file changed, 2 insertions(+)
```
##  Now checkout branch-one and make changes

```

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git checkout -b   branch-one
Switched to branch 'branch-one'

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git status
On branch branch-one
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   Branch-one.txt

no changes added to commit (use "git add" and/or "git commit -a")

```

##  Rebase

```

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git commit -m "Branch one changes"
[branch-one 99e67d3] Branch one changes
 1 file changed, 2 insertions(+), 1 deletion(-)

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-one)
$ git rebase master

```
##  Checkout master and merge
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (branch-two)
$ git checkout master
Switched to branch 'master'


Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git merge branch-one
Updating f224d14..a076e58
Fast-forward
```
-----
----
# Reset
Create a file with 5 commit lines and 5 corresponding commits.

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git log --oneline
e57239a (HEAD -> master) Commit-5
0459659 Commit-4
799c0dc Commit-3
41fccb8 Commit-2
ab4149e Commit-1
```
##  Soft
Run reset to 3rd commit
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git reset --soft 799c0dc
```
Check the file in working directory and staging area.
```
$ git diff --staged
diff --git a/DemoFile b/DemoFile
index a10940e..fcbb486 100644
--- a/DemoFile
+++ b/DemoFile
@@ -1,3 +1,5 @@
+commit-5 change
+commit-4 change
 commit-3 change
 commit-2 change
 commit-1 change
\ No newline at end of file
```
Check log to find the difference in head
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git log --oneline
799c0dc (HEAD -> master) Commit-3
41fccb8 Commit-2
ab4149e Commit-1
```
We club commit 4 and commit 5 as single commit

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git commit -m "Clubbing commit 4 and 5"
[master e0dc684] Clubbing commit 4 and 5
 1 file changed, 2 insertions(+)
```
verify
```
$ git log --oneline
e0dc684 (HEAD -> master) Clubbing commit 4 and 5
799c0dc Commit-3
41fccb8 Commit-2
ab4149e Commit-1
```
----
##  Mixed
Run a mixed reset back to commit 3
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git reset --mixed 799c0dc
Unstaged changes after reset:
M       DemoFile
```
Check working directory and staging area and commit log


Make changes in working directory to make it individual commit again. And push to staging area
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git add .
```
Commit this as commit-4
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/GitDemo (master)
$ git commit -m "Commit-4"
[master 874ee96] Commit-4
 1 file changed, 2 insertions(+)
```
##  Hard
Run a hard reset on commit-2
```
$ git reset --hard 41fccb8
HEAD is now at 41fccb8 Commit-2
```
Veridy workind directory and staging area and log/
```
$ git log --oneline
41fccb8 (HEAD -> master) Commit-2
ab4149e Commit-1
```




