##  Checking the installation

```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work
$ git --version
git version 2.31.1.windows.1
```
##  Initialising a new git repository
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work
$ mkdir demo4git

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work
$ cd demo4git

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git
$ git init
Initialized empty Git repository in C:/Users/Mk/Desktop/Work/demo4git/.git/
```
## Create a new file in the repository using VisCode
##  Check status
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        example

nothing added to commit but untracked files present (use "git add" to track)
```
##  Adding the newly created file
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$  git add example
```
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   example
```
## Configuring user information
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git config --global user.name karthik

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git config --global user.email karthik.m@classpath.io

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git config --global --list
gui.recentrepo=E:/Documents/Pradeeo/Git_trial
user.email=karthik.m@classpath.io
user.name=karthik
```
##  Commiting the file
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git commit -m "First commit"
[master (root-commit) 07a5dde] First commit
 1 file changed, 1 insertion(+)
 create mode 100644 example

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git log
commit 07a5dde4bda50b9a3d983c415db7ad97ef76d744 (HEAD -> master)
Author: karthik <karthik.m@classpath.io>
Date:   Thu May 13 12:10:41 2021 +0530

    First commit

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git log --oneline

```
## Make small change in the new file
## Add that to the staging area
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   example

no changes added to commit (use "git add" and/or "git commit -a")

```
##  Commit the changes
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git commit -m "committing the change"
[master a05539f] committing the change
 1 file changed, 2 insertions(+), 1 deletion(-)

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git log
commit a05539f0093c918d204282d173b8ba5debc80623 (HEAD -> master)
Author: karthik <karthik.m@classpath.io>
Date:   Thu May 13 12:21:16 2021 +0530

    committing the change

commit 07a5dde4bda50b9a3d983c415db7ad97ef76d744
Author: karthik <karthik.m@classpath.io>
Date:   Thu May 13 12:10:41 2021 +0530

    First commit
```
-------
------

## Make another change in the first file, and also create a new file.
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$  git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   example

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        SecondFile

no changes added to commit (use "git add" and/or "git commit -a")
```
##  Add these to staging
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git add .
```
## To unstage
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git restore --staged example

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   SecondFile

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   example
```
## To undo in working directory
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$  git restore example

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   SecondFile
```
------
-----
##  Log and check the commits to checkout
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git log --oneline
a05539f (HEAD -> master) committing the change
07a5dde First commit
```
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git checkout 07a5
Note: switching to '07a5'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 07a5dde First commit
A       SecondFile
```
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git ((07a5dde...))
$ git log
commit 07a5dde4bda50b9a3d983c415db7ad97ef76d744 (HEAD)
Author: karthik <karthik.m@classpath.io>
Date:   Thu May 13 12:10:41 2021 +0530

    First commit

```
## Check the file system
```Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git ((07a5dde...))
$ git status
HEAD detached at 07a5dde
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   SecondFile
```
##  To come back 
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git ((07a5dde...))
$ git checkout master
Previous HEAD position was 07a5dde First commit
Switched to branch 'master'
A       SecondFile
```
------
------
## Branches
## To see branches present
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git branch
  experimental
* master
```
## To move to other branch
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$  git checkout experimental
Switched to branch 'experimental'
A       SecondFile
```
## Create a new branch
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (experimental)
$ git branch newbranch

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (experimental)
$ git branch
* experimental
  master
  newbranch
```

## Clubbing both
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git checkout -b featurebranch
Switched to a new branch 'featurebranch'

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git branch
  experimental
* featurebranch
  master
  newbranch
```

## After checking out to a branch, create a new file
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git status
On branch featurebranch
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   SecondFile

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        Featurebranch
```
## Checkout master and check
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git checkout master
Switched to branch 'master'
A       SecondFile

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        Featurebranch

nothing added to commit but untracked files present (use "git add" to track)
```
## Check back to featurebranch and commit 
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git commit -m "In featurebranch"
[featurebranch 19e2744] In featurebranch
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 Featurebranch
```
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git add .

Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git commit -m "In featurebranch"
[featurebranch 19e2744] In featurebranch
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 Featurebranch
```
## Checkout master and check the commit log
```
Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (featurebranch)
$ git checkout master
Switched to branch 'master'


Mk@DESKTOP-5OEMBU5 MINGW64 ~/Desktop/Work/demo4git (master)
$ git log

```
