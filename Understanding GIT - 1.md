##  GitBash
-   Git Bash is an application that provides Git command line experience on the Operating System.
-   It is a command-line **shell**, A shell is a terminal application used to interface with an operating system through written commands.
##  Check the installation
```
$ git --version
```
## Initializing a Git repositorry
 If you have a project in your local system and want to control it with **Git** 


 ```
 $git init
```

If you want to get a already existing Git project on your local machine then  `clone` can be used

```
$git clone "http/SSH"
```
##  .git 
-   The . git folder contains all the information that is necessary for your project in version control and all the information about commits, remote repository address, etc. 
-   It also contains a log that stores your commit history so that you can roll back to history.
-   It is a **hidden** file.

# Git lifecycle

![](https://chercher.tech/images/git/basic-workflow.png)


--------
# Make changes
After working on the project in the local repositorty

```
$ git status
    To see details of all the files

$ git add "file name"
    to add 1 file

$ git add .
    adds all the files

$ git diff [first-branch] [second-branch]
    Shows content differences between two branches

```
##  Commit
-   The git commit command captures a **snapshot** of the project's currently staged changes.
-   Record
-   Immutable
-   3 Properties - Who, when, why 
## Configure tooling

User information needs to be configured after initialising a git local repository

```
$ git config --global user.name "[name]"
    Sets the name you want attached

$ git config --global user.email "[email address]"
    Sets the email you want attached

$ git config --global --list
    To verify the information
```
```

$ git commit -m "descriptive message abouth the commit"
    Commits the changes to the branch
```
## Logs
Lists version history for the current branch
```
$ git log

$ git log --oneline
```

-----

# Restore 
-   The "restore" command helps to unstage or even discard uncommitted local changes.

-   On the one hand, the command can be used to undo the effects of git add and unstage changes you have previously added to the Staging Area.
```
$ git restore --staged "file name"
    To unstage
```
-   On the other hand, the restore command can also be used to discard local changes in a file, thereby restoring its last committed state.

```
$ git restore "file name"
    To undo changes
```
-----
# Checkout commit

Updates files in the working tree to match the version in the index or the specified tree. 

```
$ git checkout "Commit id"

$ git checkout master
```
-----
## Commit Chain
![Chain image](https://miro.medium.com/max/2540/1*tFGUQ9I0DYY3NBk2ZBY97A.png)
-   Commits are the smallest denominator, git only has significance of commit.
-    The file is stored in the .git/objects/info directory
 if the commit-graph-chain file contains the lines. 
 -  These **hash** are formed by the content of commit. Can be related thought reversed linked list.
```
	{hash0}
	{hash1}
	{hash2}
```


then the commit-graph chain looks like the following diagram:
```
+-----------------------+
|  graph-{hash2}.graph  |
+-----------------------+
  |
+-----------------------+
|                       |
|  graph-{hash1}.graph  |
|                       |
+-----------------------+
  |
+-----------------------+
|                       |
|                       |
|                       |
|  graph-{hash0}.graph  |
|                       |
|                       |
|                       |
+-----------------------+
```
----
## Branches

-   Branches are an important part of working with **Git**. Usually for different tasks/teams different branches are created.<br>
-   **Master** branch  
-   A branch is created to work on a **new** feature.
-   Commits are specific to each branch.

![](https://www.nobledesktop.com/image/gitresources/git-branches-merge.png)

```
$ git branch "branch-name"
    Creates a new branch

$ git checkout "branch-name"
    Switches to a specific branch

$ git checkout -b "branch-name"
    Creates a new branch and also checksout

$ git branch -d "branch-name"
    Deletes the specific branch
```
-----





















----

The git revert command can be considered an 'undo' type command. Reverting should be used when you want to apply the inverse of a commit from your project history.
![](https://wac-cdn.atlassian.com/dam/jcr:b6fcf82b-5b15-4569-8f4f-a76454f9ca5b/03%20(7).svg?cdnVersion=1587)
```
$ git revert HEAD
```
----

## SSH Key
-   Using the SSH protocol, you can connect and authenticate to remote servers and services. 
-   With SSH keys, you can connect to GitLab **without** supplying your username and personal access at each visit.

SSH key has to be generated and added to your GitLab account


```
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/schacon/.ssh/id_rsa):
Created directory '/home/schacon/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/schacon/.ssh/id_rsa.
Your public key has been saved in /home/schacon/.ssh/id_rsa.pub.
The key fingerprint is:
d0:82:24:8e:d7:f1:bb:9b:33:53:96:93:49:da:9b:e3 schacon@mylaptop.local
```
Now you have to go to the location _/home/schacon/.ssh_ and open the content of the **.pub** file copy it.
Paste this key on you Gitlab account in preferences.

----


## Synchronizing the changes
After commiting the changes on the local repository, it has to be synchronized with remote repository on GitLab
```
$ git fetch
    Download all history from remote tracking branches

$ git merge
    Combines remote tracking branch into current local branch

$ git push
    Uploads all local brnaches to Gitlab

$ git pull
    Updates your local working branch with all new commits from corresponding remote on Gitlab
```
-----
## Wortking with Remotes (Git remote commands)
-   Remote repositories are versions of your project that are hosted on the Internet or network somewhere. 
-   Collaborating with others involves managing these remote repositories and **pushing** and **pulling** data to and from them when you need to share work.

- Showing your remotes
    
    -   To see which remote servers you have configured, you can run  `$git remote` command. It  It lists the shortnames of each remote handle you’ve specified. <br> 
    -   If you’ve cloned your repository, you should at least see `origin`, that is the default name **Git** gives to the server you cloned from.
    ```
    $ git clone https://github.com/schacon/ticgit

    $ cd ticgit

    $ git remote
    origin

    $git remote -v
    origin	https://github.com/schacon/ticgit (fetch)
    origin	https://github.com/schacon/ticgit (push)
    ```
    If -v is specified, it shows you the URLs along with the shortnames.
- Adding remote repositories
    
    To add a new remote **Git** repository as a **short name** that can be easily referenced easily
    ```
    $ git remote add pb https://github.com/paulboone/ticgit
    $ git remote -v
    origin	https://github.com/schacon/ticgit (fetch)
    origin	https://github.com/schacon/ticgit (push)
    pb	https://github.com/paulboone/ticgit (fetch)
    pb	https://github.com/paulboone/ticgit (push)
    ```
- Pulling from remotes

    -   If your current branch is set up to track a remote branch  you can use the **git pull** command to automatically fetch and then merge that remote branch into your current branch.<br>
    -   And by default, the git _clone_ command automatically sets up your local **master** branch to track the remote **master**
    
    The command to pull from a remote branch and merge with your local branch is
    ```
    $ git pull <remote> <branch>

    $ git pull origin master
        Pulls from master
    ``` 
- Pushing to remotes

    When you have your project at a point that you want to share, you have to push it upstream. The command for this is
    ```
    $ git push <remote> <branch>

    $ git push origin master
        Pushes to master branch
    ```
- Renaming and Removing Remotes
    
    
    For instance, if you want to rename pb to paul, you can do so with git remote rename
    ```
    $ git remote rename pb paul
    
    $ git remote
    origin
    paul
    ```
    If you want to remove a remote for some reason 
    ```
    $ git remote remove paul
    
    $ git remote
    origin
    ```
-----

    
## Tags

    
-   Tag assigns a meaningful name with a **commit specific version** in the repository. 
-   Tags are very similar to branches, but the difference is that tags are **immutable**. It means, tag is a branch, which nobody intends to modify.
- Usually, developers create tags for **product releases**.

Listing tags
```
$ git tag
v1.0
v2.0
```
Creating tag
```
$ git tag v1.4
```
Sharing tags
```
$ git push origin v1.4
Counting objects: 14, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (12/12), done.
Writing objects: 100% (14/14), 2.05 KiB | 0 bytes/s, done.
Total 14 (delta 3), reused 0 (delta 0)
To git@github.com:schacon/simplegit.git
 * [new tag]         v1.4 -> v1.4

$ git push origin --tags
    If you have lot of tags that you want to push up at once.
 ```
Deleting Tags
```
$ git tag -d v1.4
Deleted tag 'v1.4'
```

----










 

    



