Version control systems are a category of software tools that helps in recording changes made to files by keeping a track of modifications done to the code. 

##  Why
-   As we know that a software product is developed in **collaboration** by a group of developers they might be located at different locations and each one of them contributes in some specific kind of functionality/features. 
-   A version control system is a kind of software that helps the developer team to efficiently **communicate and manage(track) all the changes** that have been made to the source code along with the information like who made and what change has been made.
-   It not only keeps source code organized but also improves productivity by making the development process smooth.

##  Benefits
-    Enhances the project development speed by providing efficient collaboration
-   Reduce possibilities of **errors and conflicts** meanwhile project development through **traceability** to every small change
-   For each different contributor of the project a **different working copy** is maintained and not merged to the main file unless the working copy is validated.
-   Helps in **recovery** in case of any disaster or contingent situation.
-    Authentication & Authorization
-   A **repository** - It can be thought of as a database of changes. It contains all the edits and historical versions (snapshots) of the project.
-   **Versioning** all the changes.

-----
##  Centralized Version Control Systems
Centralized version control systems contain just one repository and each user gets their own working copy. Eg : SVN, perforce

![](https://media.geeksforgeeks.org/wp-content/uploads/20190624140224/cvcss.png)

Two things are required to make your changes visible to others which are: 
 

1.  You commit
1.  They update

It has some **downsides** as well which led to the development of DVS.
-   The most obvious is the **single point of failure**
-   Branching will be costly

##  Distributed Version Control System
-   Distributed version control systems contain multiple repositories. Each user **has their own repository** and working copy. Eg: Git, Mercurial
-   Just committing your changes will not give others access to your changes.

![](https://media.geeksforgeeks.org/wp-content/uploads/20190624140226/distvcs.png) 

-   To make your changes visible to others, 4 things are required: 
 

1.  You commit
1.  You push
1.  They pull
1.  They update

-   Multiple people can work simultaneously on a single project. Everyone works on and edits their own copy of the files
-   Speed
-   Branches are cheap
- Every operation is local
-----
Install from git-scm